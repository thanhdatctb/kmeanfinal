from django.db import models

# Create your models here.\
class ViTri(models.Model):
    MaViTri = models.CharField(max_length=200, primary_key=True)
    TenViTri = models.TextField(max_length=200)


class ChuongTrinhDaoTao(models.Model):
    MaCTDT = models.TextField(max_length=200, primary_key=True)
    MaMonHoc =models.TextField(max_length=200)
    TenMonHoc = models.TextField(max_length=200)
    TinChi = models.IntegerField()


class KhoaDaoTao(models.Model):
    MaKhoaDaoTao = models.TextField(max_length=200, primary_key=True)
    TenKhoaDaoTao = models.TextField(max_length=200)
    MaCTDT = models.TextField(max_length=200)


class MonHocTheoViTri(models.Model):
    STTMonHoc = models.IntegerField()
    STTMonHoc1 = models.IntegerField()
    STTMonHoc2 = models.IntegerField()
    STTMonHoc3 = models.IntegerField()
    STTMonHoc4 = models.IntegerField()
    STTMonHoc5 = models.IntegerField()
    MaKhoaDaoTao = models.TextField(max_length=200)
    MaViTri = models.CharField(max_length=200)


class DiemSinhVien(models.Model):
    MaSV = models.TextField(max_length=200)
    STTMonHoc1 = models.IntegerField()
    STTMonHoc2 = models.IntegerField()
    Name = models.TextField(max_length=200)
    Name2 = models.TextField(max_length=200)
    Name3 = models.TextField(max_length=200)
    Name4 = models.TextField(max_length=200)
    Name5 = models.TextField(max_length=200)
    Name6 = models.TextField(max_length=200)
    Name7 = models.TextField(max_length=200)
    MaKhoaDaoTao = models.TextField(max_length=200)
    MaViTri = models.CharField(max_length=200)


class MaSinhVienLuuTru(models.Model):
    MaSV = models.TextField(max_length=200, primary_key=True)
    STTMonHoc1 = models.IntegerField()
    STTMonHoc2 = models.IntegerField()
    STTMonHoc3 = models.IntegerField()
    Name = models.TextField(max_length=200)
    Name2 = models.TextField(max_length=200)
    Name3 = models.TextField(max_length=200)
    Name4 = models.TextField(max_length=200)
    Name5 = models.TextField(max_length=200)
    MaKhoaDaoTao = models.TextField(max_length=200)
    MaViTri = models.CharField(max_length=200)








